#!/bin/sh
# npm install --global nodemon

# startup script for docker container. changes the DNS resolvers to use Wilsonville DNS
cat /etc/resolv.conf.restore > /etc/resolv.conf;
echo "# google ;-)" >> /etc/resolv.conf;
echo "nameserver 8.8.8.8" >> /etc/resolv.conf;
echo "nameserver 8.8.4.4" >> /etc/resolv.conf;

while read p; do
  eval "export $p";
done </usr/src/app/.env

cd /usr/src/app
export SRC_DIR="server"
npm start
# for debugging
# node -r dotenv/config  server/app.js 2>&1 | tee /var/log/app.log
