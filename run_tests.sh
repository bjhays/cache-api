# due to the fact that imports have to be static, we have to precompile to get it to work :-/
rm -rf server
npm run precompile

export WHITELIST='www.NonExistantServerB.com, www.NonExistantServer.com*, www.NonExistantServerMock.com*, www.mockServer2.com*';
export SRC_DIR="src"
export REDIS_HOST="127.0.0.1"
export WINSTON_LOG_LEVEL='debug'
TESTS=$(find tests -name '*.js')


echo "testing:
 $TESTS"

./node_modules/mocha/bin/_mocha -t 6000 --compilers js:babel-register $TESTS $OPT
# --bail --full-trace 
