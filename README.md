Cache-api
==========
Cache-api

Getting Started
---------------
Run locally:
  Need node 6.0
  create .env file in root


Optional:
  nodemon - monitor code for changes
  # npm install -g nodemon

.. code-block:: bash

    $ npm install;
    $ node -r dotenv/config node_modules/.bin/babel-node src/app.js | tee log.log
    or
    $ nodemon -r dotenv/config node_modules/.bin/babel-node src/app.js | tee log.log

Build Locally
.. code-block:: bash

    $ npm run "precompile"
    # add hostname as the redis host
    $ docker build -t local:cache-api .


Docker Compose
.. code-block:: bash

    $ docker compose up


Environment Vars
---------------
    # setup
    LOAD_BALANCER_SECURITY_GROUP: '',
    SUBNET_ONE: '',
    SUBNET_TWO: '',
    SUBNET_THREE: '',
    ECS_HOST_PORT: '',
    LOAD_BALANCER_PORT: '',
    DOCKER_REGISTRY: '',
    npm_package_version: '0.0.1',
    DOCKER_TAG: '',
    ECS_CLUSTER: '',
    ENV: 'dev',
    PROJECT_NAME: 'cache-api',
    # app
    REDIS_HOST: process.env.REDIS_HOST || '127.0.0.1',
    REDIS_PORT: process.env.REDIS_PORT || 6379,
    SWAGGER_MOCK_MODE: process.env.SWAGGER_MOCK_MODE || false,
    NODE_API_KEY: process.env.NODE_API_KEY || 'foo',
    WHITELIST: process.env.WHITELIST.split(',') || [] # comma delimeted list
    PROXY_CACHE_DEFAULT: process.env.PROXY_CACHE_DEFAULT || 60, # sec
    WINSTON_LOG_LEVEL: process.env.WINSTON_LOG_LEVEL || 'error'

Docker
---------------
    docker build -t local:cache-api .
    docker run -i -t local:cache-api /bin/bash # interactive
    docker run -p 3000:3000 -t local:cache-api # default

Ubuntu/Debian installation
---------------
.. code-block:: bash
      $ sudo apt-get install build-essential checkinstall
      $ sudo apt-get install libssl-dev
      $ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash
      $ command -v nvm
      #close window/ reopen
      $ nvm install 5.0
      $ nvm use 5.0
      $ nvm alias default node


DNS
---------------
nameservers.txt will be appended to the default google dns on container start to allow wilsonville DNS names to be resolved


Proxy
---------------

<this_server>/proxy/http://your_end_point?cache_duration=60&request_time_out=300&your_other_param=1

optional params:
 * cache_duration [default: 60 sec] - time in sec for L1 cache to hold. If 0 cache is not used at all (not even L2)
 * request_time_out [default: 300 ms] - time in ms that the request to a server will try before deeming the server not reachable.

 returns:
 * proxies your_end_point body, headers and return code
 * adds headers:
 ** _proxy_cache L1:hit or L2:hit depending on cache (only valid if cache_duration > 0)
 ** _proxy_procTime amount of time that the request took to process
 ** _proxy_getSource amount of time that the request to source took to process
