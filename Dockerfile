FROM node:argon

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Bundle app source
COPY . /usr/src/app

RUN cat /usr/src/app/nameservers.txt > /etc/resolv.conf.restore

EXPOSE 3000
CMD [ "/usr/src/app/docker_run.sh" ]
