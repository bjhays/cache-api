rm -rf server
npm run precompile

echo "node: $(node -v)"
echo "npm : $(npm -v)"
export $(cat .env | xargs)
npm install
export SRC_DIR="src"
export REDIS_HOST='127.0.0.1'

nodemon -r dotenv/config node_modules/.bin/babel-node src/app.js 2>&1 | tee log.log
