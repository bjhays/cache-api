/* Required environment variables:
   PROJECT_NAME
   ENV
   DOCKER_REGISTRY
   DOCKER_TAG
   ECS_CLUSTER
   ECS_DESIRED_COUNT
   ECS_HOST_PORT
   ECS_ROLE
   HOSTED_ZONE_ID - The hosted zone for the DNS alias
   DNS_ROUTE - Name of the DNS route name
*/
/*eslint-disable */
const fs = require('fs');
fs.access('./server/lib/config.js', fs.F_OK, function(err) {
    if (err) {
        throw Error('./server/lib/config.js not found, need to precompile');
    }
});


const aws = require('aws-sdk'),
    exec = require('child_process').execSync,
    gulp = require('gulp'),
    config = require('./server/lib/config');

var tag = function(ver){
    if (ver==undefined) {
        throw Error("version undefined");
    }
    var remote = config.DOCKER_REGISTRY + '/' + config.PROJECT_NAME + ':' + ver;
    exec('docker tag ' + config.PROJECT_NAME + ' ' + remote);
    exec('docker push ' + remote);
};

gulp.task('elb:create-load-balancer',function(done){
    var elb = new aws.ELB();
    var subnets = [];
    if (config.SUBNET_ONE) {
        subnets.push(config.SUBNET_ONE);
    }

    if (config.SUBNET_TWO) {
        subnets.push(config.SUBNET_TWO);
    }

    if (config.SUBNET_THREE) {
        subnets.push(config.SUBNET_THREE);
    }



    var params = {
        LoadBalancerName: config.PROJECT_NAME + '-' + config.ENV,
        Listeners: [
            {
                InstancePort: config.ECS_HOST_PORT,
                LoadBalancerPort: config.LOAD_BALANCER_PORT || 80,
                Protocol: 'HTTP'
            }
        ],
        Scheme: 'internal',
        SecurityGroups: [
            config.DEV_ALL_SECURITY_GROUP,
            config.LOAD_BALANCER_SECURITY_GROUP
        ],
        Subnets: subnets,
        Tags: [
            { "Key": "environment", "Value": config.ENV },
        ]
    };
    console.log(params)
    elb.createLoadBalancer(params,function(err,data) {
        if(err){
          console.log(err);
        }
        var params = {
            LoadBalancerName: config.PROJECT_NAME + '-' + config.ENV,
            LoadBalancerAttributes: {
                ConnectionDraining: {
                    Enabled: true,
                    Timeout: 30
                },
                CrossZoneLoadBalancing: {
                    Enabled: true
                },
                ConnectionSettings: {
                    IdleTimeout: 3600
                }
            }
        };
        setTimeout(
            function(){
                elb.modifyLoadBalancerAttributes(params,done);
            },
            1000
        );
    });
});

gulp.task('elb:delete-load-balancer',function(done){
    var elb = new aws.ELB();
    var params = {
        LoadBalancerName: config.PROJECT_NAME + '-' + config.ENV
    };
    elb.deleteLoadBalancer(params,done);
});

gulp.task('elb:configure-healthcheck',['elb:create-load-balancer'],function(done){
    var elb = new aws.ELB();
    var params = {
        LoadBalancerName: config.PROJECT_NAME + '-' + config.ENV,
        HealthCheck: {
            HealthyThreshold: 2,
            Interval: 5,
            Target: 'HTTP:'+config.ECS_HOST_PORT+'/v1/ping',
            Timeout: 2,
            UnhealthyThreshold: 2
        }
    };
    elb.configureHealthCheck(params, done);
});

gulp.task('ecs:register-task-definition',['elb:create-load-balancer'],function(done){
    var ecs = new aws.ECS();
    var env = [];
    for(var i in config){
        env.push({ name: i, value: process.env[i] });
    }
    var params = {
        family: config.PROJECT_NAME + '-' + config.ENV,
        containerDefinitions: [
            {
                name: 'node',
                image: config.DOCKER_REGISTRY + '/' + config.PROJECT_NAME + ':' + config.DOCKER_TAG,
                cpu: 100,
                memory: 256,
                essential: true,
                environment: env,
                portMappings: [
                    {
                        containerPort: config.ECS_CONTAINER_PORT,
                        hostPort: config.ECS_HOST_PORT
                    }
                ]
            }
        ]
    };
    console.log(params)
    ecs.registerTaskDefinition(params, done);
});

gulp.task('ecs:create-service',['ecs:update-service'],function(done){
    var ecs = new aws.ECS();
    var params = {
        serviceName: config.PROJECT_NAME + '-' + config.ENV,
        desiredCount: config.ECS_DESIRED_COUNT,
        taskDefinition: config.PROJECT_NAME + '-' + config.ENV,
        loadBalancers: [
            {
                containerName: 'node',
                containerPort: config.ECS_CONTAINER_PORT,
                loadBalancerName: config.PROJECT_NAME + '-' + config.ENV
            }
        ],
        cluster: config.ECS_CLUSTER,
        role: config.ECS_ROLE
    };
    console.log(params);
    ecs.createService(params,function(err,data){
        if(err){
          console.log(err);
        }
        done();
    });
});

gulp.task('ecs:update-service',['ecs:register-task-definition'],function(done){
    var ecs = new aws.ECS();
    var params = {
        service: config.PROJECT_NAME + '-' + config.ENV,
        desiredCount: config.ECS_DESIRED_COUNT,
        taskDefinition: config.PROJECT_NAME + '-' + config.ENV,
        cluster: config.ECS_CLUSTER
    };
    console.log(params);
    ecs.updateService(params,function(err,data){
        if(err){
          console.log(err);
        }
        done();
    });
});

gulp.task('ecs:wait-for-stable',['ecs:create-service'],function(done){
    var ecs = new aws.ECS();
    var params = {
        services: [
            config.PROJECT_NAME + '-' + config.ENV
        ],
        cluster: config.ECS_CLUSTER
    };
    ecs.waitFor('servicesStable', params, done);
});

gulp.task('route53:change-dns-target',['ecs:wait-for-stable'],function(done){
  var elb = new aws.ELB();
  var params = {
      "LoadBalancerNames": [config.PROJECT_NAME + '-' + config.ENV]
  };
  elb.describeLoadBalancers(params,function(err, data){
    if(err){
      console.log(err);
    }
    var route53 = new aws.Route53();
    var params = {
      "HostedZoneId": config.HOSTED_ZONE_ID,
      "ChangeBatch": {
        "Changes": [
          {
            "Action": "UPSERT",
            "ResourceRecordSet": {
              "Name": config.DNS_ROUTE,
              "Type": "A",
              "AliasTarget": {
                  "DNSName": data.LoadBalancerDescriptions[0].DNSName,
                  "EvaluateTargetHealth": false,
                  "HostedZoneId": data.LoadBalancerDescriptions[0].CanonicalHostedZoneNameID
              }
            }
          }
        ]
      }
    };
    route53.changeResourceRecordSets(params, function(err,data) {
      if(err){
        console.log(err);
      }
    });
  });


});

gulp.task('ecs:delete-service',function(done){
    var ecs = new aws.ECS();
    var params = {
        service: config.PROJECT_NAME + '-' + config.ENV,
        desiredCount: 0,
        cluster: config.ECS_CLUSTER
    };
    ecs.updateService(params,function(err,data){
        ecs.deleteService({
            service: params.service,
            cluster: params.cluster
        }, done);
    });
});

gulp.task('bootstrap',function(){
    console.log('if I had anything to pre-init before qa tests could run, I would do it here');
});

gulp.task('deploy',['elb:create-load-balancer','elb:configure-healthcheck','ecs:wait-for-stable','route53:change-dns-target']);

gulp.task('package',function(){
    exec('docker build -t ' + config.PROJECT_NAME + ' .');
    tag(config.DOCKER_TAG);
});

gulp.task('tag',function(){
    tag(config.npm_package_version);
});

gulp.task('clean', ['ecs:delete-service','elb:delete-load-balancer']);
