import winston from 'winston';
import redis from 'redis';
import axios from 'axios';
import url from 'url';
import crypto from 'crypto';

// redis.debug_mode = true;
winston.level = 'error';

function getChecksum(str, algorithm = null, encoding = null) {
  return crypto
      .createHash(algorithm || 'md5')
      .update(str, 'utf8')
      .digest(encoding || 'hex');
}

/**
  * create a standardized redis key
  *
  * @param level {Number}
  * @param method {String}
  * @param headers {Object}
  * @param method {Body}
  * @return {String}
  *
  */

function getRedisKey(level, method, uri, headers = {}, body = '') {
  if (uri.indexOf('http://') < 0) {
    uri = `http://${uri}`;
  }

  const urlParts = url.parse(uri, true);
  // console.log(urlParts);
  let bodyCS = 'none';
  if (body) {
    bodyCS = getChecksum(body);
  }
  const key = `L${level}:${method.toUpperCase()}:${urlParts.host}:${urlParts.path.slice(1).replace('/', '_')}${urlParts.search.replace('?', '')}:${bodyCS}`;
  winston.log('debug', `redisKey: ${key}`);
  return key;
}

/**
  * given axios response object, create a base object that can be stored in redis
  *
  * @param response {Object} =
  *   {
  *     data: {String}
  *     headers: {Object}
  *     status: {Number}
  *   }
  *
  * @return {String}
  *    a json string representation of an object
  *
  */

function cacheObj(object) {
    winston.log('debug', `cacheObj: ${object}`);
    return JSON.stringify({ data: object.data, headers: object.headers, status: object.status });
}

exports.getRedisKey = getRedisKey;
exports.cacheObj = cacheObj;


class Cache {
  constructor(host = null) {
    this._port = 6379;
    this._host = null;
    this._redisClient = null;
    this.DEFAULT_CACHE_DURATION = 60; // sec
    this.DEFAULT_REQUEST_TIMEOUT = 300; // ms

    if (host) {
      this._host = host;
      this.connect((err, redisClient) => {
          if (err) {
            winston.log('error', err);
          }
          winston.log('debug', redisClient);
          this._redisClient = redisClient;
      });
    }
  }

  /**
  * connect to redis, save in class var
  */

  connect(callback) {
    if (!callback) {
      winston.log('error', 'connect supplied invalid callback');
      throw Error('callback');
    }
    let redisClient = this._redisClient;
    if (redisClient === null || redisClient === undefined) {
        winston.log('debug', `Connect to redis: ${this._host}:${this._port}`);
        try {
          redisClient = redis.createClient(this._port, this._host, { connect_timeout: 10000 });
          redisClient.on('connect', () => {
              if (redisClient === null || redisClient === undefined) {
                winston.log('error', 'failed to connect to redis');
                return callback('failed to connect to redis', null);
              } else {
                this._redisClient = redisClient;
                return callback(null, redisClient);
              }
          });
          redisClient.on('error', (err) => {
            winston.log('error', 'cache connect: redisClient Error');
            winston.log('error', err);
            callback('failed to connect to redis', err);
          });
        } catch (e) {
          return callback(e.message, null);
        }
    } else {
      return callback(null, redisClient);
    }
  }

  set cacheDuration(sec) {
    this.DEFAULT_CACHE_DURATION = sec;
  }

  set requestTimeout(ms) {
    this.DEFAULT_REQUEST_TIMEOUT = ms; // ms
  }

  set allowed(allowed) {
    this._allowed = allowed;
  }

  set client(client) {
    winston.log('debug', 'set client');
    // console.log(`set client: ${client}`);
    this._redisClient = client;
  }

  set host(host) {
    this._host = host;
  }

  set port(port) {
    this._port = port;
  }

  toString() {
    const ret = `port:${this._port}/host:${this._host}/redisClient:${this._redisClient}`;
    return `cache class: ${ret}`;
  }

  /**
  * create a standardized redis key
  *
  * @param level {Number}
  * @param method {String}
  * @param headers {Object}
  * @param method {Body}
  * @return {String}
  *
  */

  getRedisKey(level, method, uri, headers = {}, body = '') {
    return getRedisKey(level, method, uri, headers, body);
  }

  /**
  * given axios response object, create a base object that can be stored in redis
  *
  * @param response {axios.response}
  * @return {String}
  *    a json string representation of an object
  *
  */

  cacheObj(response) {
      return cacheObj(response);
  }

  /**
  * given axios response object, writeback cache to L1 cache, only for 200 response and populated response objects
  *
  * @param response {axios.response}
  * @param expiration {Number}
  *
  */

  setL1(response, expiration, callback) {
    // level 1 is used if the cacheDuration has not expired
    this.connect((error, redisClient) => {
      if (error) {
        return callback(error, false);
      } else {
        const key = this.getRedisKey(1, response.config.method, response.config.url, response.headers, response.body);
        winston.log('debug', `set L1 key ${key}`);
        redisClient.set(key, this.cacheObj(response), (err) => { if (err) { winston.log('error', `setL1: ${err}`); } });
        redisClient.expire(key, expiration);
        return callback(null, true);
      }
    });
  }

  /**
  * given axios response object, writeback cache to L2 cache, only for 200 response and populated response objects
  *
  * @param response {axios.response}
  * @param expiration {Number}
  *
  */

  setL2(response, callback) {
    // Second layer cache that does not expire for working around GTS downtime
    this.connect((error, redisClient) => {
      if (error) {
        return callback(error, false);
      } else {
        const key = this.getRedisKey(2, response.config.method, response.config.url, response.headers, response.body);
        winston.log('debug', `set L2 key ${key}`);
        redisClient.set(key, this.cacheObj(response), (err) => { if (err) { winston.log('error', `setL2: ${err}`); } });
        return callback(null, true);
      }
    });
  }

  /**
  * given axios response object, writeback cache to L1 and L2 cache, only for 200 response and populated response objects
  *
  * @param response {axios.response}
  * @param expiration {Number}
  *
  */

  writeBack(response, expiration) {
    if (response) {
      if (response.status !== 200) {
        winston.log('warn', response.status);
      }
      try {
        this.setL1(response, expiration, (err, data) => { if (err) { winston.log('error', err); } });
        this.setL2(response, (err, data) => { if (err) { winston.log('error', err); } });
      } catch (e) {
        winston.log('error', e);
      }
    } else {
      winston.log('warn', 'can\'t cache empty response');
    }
    winston.log('debug', 'writeback done');
  }

  getL1(payload, callback) {
    this.connect((error, redisClient) => {
      if (error) {
        return callback(error, false);
      } else {
        const key = this.getRedisKey(1, payload.method, payload.uri, payload.headers, payload.body);
        redisClient.get(key, (err, reply) => {
            // reply is null when the key is missing
            if (reply !== null) {
              // 2do - validate ?
              winston.log('debug', 'getL1 - cache hit');
              const ret = JSON.parse(reply);
              // winston.log('debug', ret);
              ret.headers._proxy_cache = 'L1:hit';
              return callback(null, ret);
            } else {
              return callback('getL1 - cache miss - not found', null);
            }
          });
        }
    });
  }

  getL2(payload, callback) {
    this.connect((error, redisClient) => {
      if (error) {
        return callback(error, false);
      } else {
        const key = this.getRedisKey(2, payload.method, payload.uri, payload.headers, payload.body);
        redisClient.get(key, (err, reply) => {
            // reply is null when the key is missing
            if (reply !== null) {
              // 2do - validate ?
              winston.log('debug', 'getL2 - cache hit');
              const ret = JSON.parse(reply);
              ret.headers._proxy_cache = 'L2:hit';
              return callback(null, ret);
            } else {
              return callback('getL2 - cache miss - not found', null);
            }
          });
      }
    });
  }

  /**
  * make an http call based on the payload object, writeback cache the results (if cache duration is >0) and return headers and content
  * @param payload   { method: {String}, # [GET|POST]
  *                   headers: {Object}, # any valid http header key/val pair
  *                   body: {String},    # for POST, body of request
  *                   uri: {String},     # full path to http endpoint, including query params
  *                   cacheDuration: {Number}, # time in sec that the result will remain in L1 cache
  *                   requestTimeout: {Number} } # time in ms that the request will try the http endpoint before returning L2 or an error
  *
  * @param callback
  *    function to handle (err, response)
  *    @param err {String}
  *       an error message
  *    @param response
  *       a response object defined in cacheObj function above
  *
  * @return None
  */

  getSource(payload, callback) {
    const start = Date.now();
    if (!payload.method) {
      payload.method = 'get';
    }

    if (payload.headers) {
      if ('user-agent' in payload.headers) {
        delete(payload.headers['user-agent']);
      }
      if ('connection' in payload.headers) {
        delete(payload.headers['connection']);
      }
      if ('accept' in payload.headers) {
        delete(payload.headers['accept']);
      }
    } else {
      payload.headers = {};
    }
    payload.headers['accept-encoding'] = '*/*';

    winston.log('debug', 'payload:');
    winston.log('debug', payload);
    winston.log('debug', payload.headers);

    if (!payload.body) {
        payload.body = '';
    }

    if (!payload.cacheDuration) {
        payload.cacheDuration = this.DEFAULT_CACHE_DURATION; // sec
    }

    if (!payload.requestTimeout) {
        payload.requestTimeout = this.DEFAULT_REQUEST_TIMEOUT; // ms
    }

    if (payload.uri.indexOf('http://') < 0) {
      payload.uri = `http://${payload.uri}`;
    }

    // middleware/express add headers that we don't want
    const removeHeaders = ['uri', 'host', 'dnt', 'cache-control', 'host', 'upgrade-insecure-requests', 'accept-language'];
    removeHeaders.forEach((key) => {
      if (payload.headers[key]) {
        console.log(`Deleting ${key}`);
        delete(payload.headers[key]);
      }
    });

    winston.log('debug', payload.headers);

    winston.log('debug', 'call axios');
    const options = {
        method: payload.method,
        url: payload.uri,
        data: payload.body,
        timeout: payload.requestTimeout,
        headers: payload.headers
      };
    winston.log('debug', options);
    axios(options.url, options)
      .then((response) => {
        winston.log('have response');
        if (response) {
          winston.log('debug', `getSource - response ${response.status}`);
          if (payload.cacheDuration > 0) {
              try {
                  this.writeBack(response, payload.cacheDuration);
              } catch (e) {
                  winston.log('error', `writeBack: ${e.message}`);
              }
          }
          winston.log('debug', `response from source: ${response.data}`);
          response.headers._proxy_getSource = (Date.now() - start);
          return callback(null, response);
        } else {
          winston.log('warn', 'empty response');
          return callback('empty response', null);
        }
      }).catch(err => {
        winston.log('error', 'caught axios err:');
        try {
          winston.log('error', `${err.status}:${err.statusText}\n${err.data}`);
          // winston.log('error', err);
        } catch (e) {
          winston.log('error', err);
        }
        return callback(err, null);
      });
      return;
  }

  /*
  * check a uri against a whitelist to allow calling to the endpoint
  *
  * @param uri {String}
  *    full string including query params
  * @param callback {Function}
  *    function to handle error/success conditions
  * @return {Boolean}
  */

  allowCache(uri, callback) {
    winston.log('debug', `uri: ${uri}`);
    const endpointClean = uri.replace('http://', '');
    if (this._allowed.indexOf(endpointClean) < 0) {
      winston.log('debug', `whitelist: '${this._allowed}'`);
      // regex
      for (let i = this._allowed.length - 1; i >= 0; i--) {
          const rule = this._allowed[i].trim();
          if (endpointClean.match(rule)) {
              winston.log('debug', `${endpointClean} matches ${rule}`);
              callback(null, true);
              return true;
          }
      }
      winston.log('error', `${endpointClean} not in whitelist`);
      callback({ statusText: `${endpointClean} not in whitelist` }, null);
      return false;
    }
    winston.log('debug', `${endpointClean} is in whitelist`);
    return callback(null, `${endpointClean} is in whitelist`);
  }

  /**
  * Given payload, do logic to:
  *     Fetch an L1 cached response if exists and cacheDuration >0
  *     Fetch source response if above fails or returns null
  *     Fetch L2 response if both of above fail
  *     throw error if all above fail
  *
  * @param payload   { method: {String}, # [GET|POST]
  *                   headers: {Object}, # any valid http header key/val pair
  *                   body: {String},    # for POST, body of request
  *                   uri: {String},     # full path to http endpoint, including query params
  *                   cacheDuration: {Number}, # time in sec that the result will remain in L1 cache
  *                   requestTimeout: {Number} } # time in ms that the request will try the http endpoint before returning L2 or an error
  *
  * @param callback
  *    function to handle (err, response)
  *    @param err {String}
  *       an error message
  *    @param response
  *       a response object defined in cacheObj function above
  *
  * @return None
  */

  getData(payload, callback) {
    this.allowCache(payload.uri, (errAllow, data) => {
      if (errAllow) {
        return callback(errAllow, null);
      }

      if (! payload.cacheDuration) {
        payload.cacheDuration = this.DEFAULT_CACHE_DURATION;
      }

      if (!payload.requestTimeout) {
        payload.requestTimeout = this.DEFAULT_REQUEST_TIMEOUT; // ms
      }

      if (! payload.headers) {
        payload.headers = {};
      }
      payload.headers['accept-encoding'] = '*/*';

      winston.log('debug', `${payload.uri} allowed: ${data}`);
      if (payload.cacheDuration > 0) {
        // if cacheDuration > 0, pull from L1
        try {
            winston.log('debug', 'try L1');
            this.getL1(payload, (errL1, responseL1) => {
                winston.log('debug', 'got response from L1');
                if (errL1) {
                  errL1 = errL1.toString();
                  winston.log('debug', errL1);
                  // cache miss, get from source
                  winston.log('debug', `getData - cache miss: ${errL1}`);
                  this.getSource(payload, (err, response) => {
                      if (err) {
                          winston.log('error', `getData/getSource - ${err.code} ${err.message}`);
                          this.getL2(payload, (errL2, responseL2) => {
                              if (errL2) {
                                // everything failed :-(
                                winston.log('error', `errL2: ${errL2}`);
                                return callback(err, null);
                                throw Error(err);
                              } else {
                                console.log('not errL2');
                                if (! responseL2) {
                                  winston.log('error', 'responseL2 is null');
                                  return callback('responseL2 is null', null);
                                } else {
                                  winston.log('debug', 'responseL2 good');
                                  return callback(null, responseL2);
                                }
                              }
                          });
                      } else {
                        if (! response) {
                          winston.log('error', 'response is null');
                          return callback('response is null', null);
                        } else {
                          winston.log('debug', 'response is good');
                          return callback(null, response);
                        }
                      }
                  });
                } else {
                    winston.log('debug', 'send response');
                    // const ret = { status: 200, headers: response.headers, data: response.data };
                    if (! responseL1) {
                      winston.log('error', 'response is null');
                      return callback(null, responseL1);
                    }
                    return callback(null, responseL1);
                }
            });
        } catch (e) {
            winston.log('error', `getData catch- ${e.message}`); // : ${e.stack}`);
            this.getSource(payload, (err, response) => {
                if (err) {
                    winston.log('error', `getSource returned error: ${err}`);
                    // winston.log('error', err);
                    return callback(err, null);
                } else {
                    winston.log('debug', 'getSource returned valid');
                    return callback(null, response);
                }
            });
        }
      } else {
        // if cacheDuration is 0, we do not pull from L1, just get from source
        winston.log('debug', `payload.cacheDuration: ${payload.cacheDuration}`);
        this.getSource(payload, (err, response) => {
            if (err) {
                winston.log('error', 'getSource returned error');
                return callback(err, null);
            } else {
                winston.log('debug', 'getSource returned valid');
                return callback(null, response);
            }
        });
      }
    });
  }
}

export { Cache };
