import config from './lib/config';
import express from 'express';
import cors from 'cors';
import swagger from 'swagger-tools';
import packageInfo from '../package';
import { error } from './controllers/error';
import { auth } from './controllers/security';
import swaggerJSON from '../resources/swagger.json';
import { proxyPost } from './controllers/proxy';
import { status, ping } from './controllers/diagnostics';
import winston from 'winston';

winston.level = config.WINSTON_LOG_LEVEL;

const corsObject = cors();
const app = express().use(corsObject);
let server;

/* eslint no-console: 0 */

// add the ability to have rawBody for proxy routes
app.use((req, res, next) => {
  if (req.url.indexOf('proxy') === 0) {
    next();
  }
  req.rawBody = '';
  req.setEncoding('utf8');

  req.on('data', (chunk) => {
    req.rawBody += chunk;
  });

  req.on('end', () => {
    next();
  });
});

// proxy routes are not bound by swagger / Middleware
app.get('/proxy/*', (req, res) => {
    proxyPost(req, res);
});

app.post('/proxy/*', (req, res) => {
    proxyPost(req, res);
});

app.get('/status', (req, res) => {
    status(req, res);
});

app.get('/ping', (req, res) => {
    ping(req, res);
});

swagger.initializeMiddleware(swaggerJSON, (middleware) => {
    app.use([
        middleware.swaggerMetadata(),
        (req, res, next) => {
           res.setHeader('api-verson', packageInfo.version);
           next();
        },
        middleware.swaggerSecurity({
           api_key: auth
        }),
        middleware.swaggerValidator({
            validateResponse: false
        }),
        middleware.swaggerRouter({
            controllers: `./${config.SRC_DIR}/controllers`,
            useStubs: config.SWAGGER_MOCK_MODE || false
        }),
        middleware.swaggerUi({ swaggerUi: '/v1/docs', swaggerUiDir: './resources/swagger-ui' }),
        error
    ]);
    server = app.listen(3000);
    console.log('app in "%s" listening at http://%s:%s', config.SRC_DIR, '127.0.0.1', 3000);
});


export { server };
