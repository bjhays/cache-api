export function error(err, req, res, version) {
  const errorMsg = {
    apiVersion: version,
    message: err.message,
    code: err.code
  };
  if (res.statusCode < 400) {
    res.statusCode = err.statusCode || 500;
  } else if (err.code === 'InvalidAPIRequest') {
    res.statusCode = 405;
  }
  return res.json(errorMsg);
}
