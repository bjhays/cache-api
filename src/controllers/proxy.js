import queryString from 'query-string';
import winston from 'winston';
import config from '../lib/config';
import truncate from 'truncate';
import { Cache } from '../lib/cache';

winston.level = config.WINSTON_LOG_LEVEL;
const truncateErrorAt = 20;
const proxyCache = new Cache();
proxyCache.host = config.REDIS_HOST;

Object.defineProperty(exports, '__esModule', {
  value: true
});

// helper for tests
exports.redisConnect = (client) => {
  // console.log(`client: ${client}`);
  if (client) {
    proxyCache.client = client;
  } else {
    throw Error('bad client!');
  }
};

// main function
exports.proxyPost = function (req, res) {
  // console.log(proxyCache.toString());
  proxyCache.allowed = config.WHITELIST;
  proxyCache.connect((err, rclient) => {
    if (err) { winston.log('error', 'proxyPost failed to connect to redis'); }
    if (rclient) { winston.log('debug', 'proxyPost connected to redis'); }
  });

  const endpoint = req.params['0'];
  const start = Date.now();

  // in sec
  let cacheDurationValue = config.PROXY_CACHE_DEFAULT;
  if ('cache_duration' in req.query) {
     cacheDurationValue = req.query.cache_duration;
     delete(req.query.cache_duration);
  }

  let requestTimeoutValue = 3000;
  if ('request_time_out' in req.query) {
    requestTimeoutValue = req.query.request_time_out;
    delete(req.query.request_time_out);
  }

  let queryAsString = '';
  if (req.query.length > 0) {
    queryAsString = `?${queryString.stringify(req.query)}`;
  }

  const payload = { method: req.method,
                    headers: req.headers,
                    body: req.rawBody,
                    uri: `${endpoint}${queryAsString}`,
                    cacheDuration: cacheDurationValue,
                    requestTimeout: requestTimeoutValue };

  winston.log('debug', 'payload:');
  winston.log('debug', payload);

  try {
    proxyCache.getData(payload, (err, response) => {
      if (err) {
        winston.log('error', `proxyPost: ${err.statusText}`);
        winston.log('error', err.toString());
        res.status(500)
        .send(err.statusText);
      } else {
        winston.log('debug', 'Main - callback!');
        if (! response) {
          winston.log('error', `got response: ${response.status}`);
          winston.log('error', truncate(err.toString(), truncateErrorAt));
        }
        response.headers._proxy_procTime = (Date.now() - start);

        // winston.log('debug', `ret status: ${response.status}`);
        // winston.log('debug', `ret data: ${response.data}`);
        // console.log(response);
        // winston.log('debug', `ret headers: ${response.headers}`);

        return res.status(response.status)
        .set(response.headers)
        .send(response.data);
      }
    });
  } catch (e) {
    winston.log('error', `catch: ${e}`);
    winston.log('error', truncate(e.toString(), truncateErrorAt));
    return res.status(500)
    .send(e.toString());
  }
};
