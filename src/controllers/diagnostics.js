import fs from 'fs';
import { redisTest } from '../controllers/redisTest';
import config from '../lib/config';
import { Cache } from '../lib/cache';
import axios from 'axios';

const proxyCache = new Cache();
proxyCache.host = config.REDIS_HOST;

Object.defineProperty(exports, '__esModule', {
  value: true
});

const headerOptions = {
        method: 'GET',
        url: 'http://127.0.0.1:3000',
        timeout: 3000,
        headers: {
          'accept-encoding': '*/*'
        }
      };

function getResolv(callback) {
  fs.readFile('/etc/resolv.conf', 'utf8', (err, data) => {
    if (err) {
      console.log(err);
    }
    callback(err, data);
  });
}

function hasDNS(callback) {
    fs.readFile('./nameservers.txt', 'utf8', (err, data) => {
    if (err) {
      console.log(err);
    }
    getResolv((err1, data1) => {
      const nameServers = data.split('\n');
      // console.log(nameServers);
      const stats = { found: [], missing: [] };
      nameServers.forEach((line) => {
        if (!line.startsWith('#')) {
          // console.log(line);
          if (data1.search(line) > 0) {
            stats.found.push(line);
          } else {
            stats.missing.push(line);
          }
        }
      });
      if (stats.missing[0]) {
        callback('Missing entries', stats);
      } else {
        callback(null, stats);
      }
    });
  });
}

function getSource(callback) {
  const payload = {
     method: 'GET',
     host: '127.0.0.1:50505',
     'cache-control': 'max-age=0',
     'upgrade-insecure-requests': '1',
     dnt: '1',
     'accept-encoding': '*/*',
     'accept-language': 'en-US,en;q=0.8',
     body: '',
     uri: 'http://iscaliforniaonfire.com',
     cacheDuration: 0,
     requestTimeout: '3000'
   };
   proxyCache.getSource(payload, callback);
}

function getData(callback) {
  callback(null, null);
  // const payload = {
  //    method: 'GET',
  //    host: '127.0.0.1:50505',
  //    'cache-control': 'max-age=0',
  //    'upgrade-insecure-requests': '1',
  //    dnt: '1',
  //    'accept-encoding': '*/*',
  //    'accept-language': 'en-US,en;q=0.8',
  //    body: '',
  //    uri: 'http://iscaliforniaonfire.com',
  //    cacheDuration: 0,
  //    requestTimeout: '3000'
  //  };
  // proxyCache.getData(payload, callback);
}

function testEndpoint(url, callback) {
  headerOptions.url = url;
  axios(headerOptions.url, headerOptions)
  .then((response) => {
      // console.log(response.data);
      callback(null, { uri: url, status: response.status.toString() });
  })
  .catch(err => {
      console.log('google err');
      console.log(err);
      callback(err.toString(), null);
    });
}

function cacheConnect(callback) {
  proxyCache.allowed = config.WHITELIST;
  try {
    proxyCache.connect();
    callback(null, 'healthy');
  } catch (e) {
    // winston.log('error', 'proxyPost failed to connect to redis');
    callback(e, null);
  }
}

exports.status = (req, res) => {
  const ret = {
      apiVersion: 1,
      data: {
          status: 'healthy',
          services: {},
          debug: {}
      }
  };

  ret.data.debug.whitelist = config.WHITELIST;
  ret.data.debug.proxyDebug = config.WINSTON_LOG_LEVEL;

  getSource((GSErr, GSData) => {
    if (GSErr) {
      ret.data.debug.GetSource = GSErr;
    } else if (GSData) {
      ret.data.debug.GetSource = GSData.status;
    }
    getData((GDErr, GDData) => {
      if (GDErr) {
        ret.data.debug.GetData = GDErr;
      } else if (GDData) {
        ret.data.debug.GetData = GDData.status;
      }
  testEndpoint('http://127.0.0.1:3000', (GErr, GData) => {
    if (GErr) {
      ret.data.debug.endPoint = GErr;
    } else if (GData) {
      ret.data.debug.endPoint = GData;
    }
    testEndpoint('http://www.google.com', (WLErr, WLData) => {
      if (WLErr) {
        ret.data.debug.WhitelistHosts = WLErr;
      } else if (WLData) {
        ret.data.debug.WhitelistHosts = WLData;
      }
        cacheConnect((cacheErr, cacheData) => {
          if (cacheErr) {
            ret.data.services.cache = 'unhealthy';
            ret.data.debug.cache = cacheData;
          } else if (cacheData) {
            ret.data.services.cache = 'healthy';
            ret.data.debug.cache = { redis: config.REDIS_HOST };
          }
          redisTest((err, data) => {
            if (err) {
              ret.data.services.redis = 'unhealthy';
              ret.data.debug.redis = data;
            } else if (data) {
              ret.data.services.redis = 'healthy';
              ret.data.debug.redis = { redis: config.REDIS_HOST };
            }
            hasDNS((err1, data1) => {
              if (err1) {
                console.log(`hasDNS: ${err1}`);
                ret.data.services.hasDNS = 'unhealthy';
                ret.data.debug.dns = data1;
              } else {
                ret.data.services.hasDNS = 'healthy';
              }
              console.log('done');
              return res.send(ret);
            });
          });
        });
      });
    });
    });
  });
};

exports.ping = (req, res) => {
  res.send({
      apiVersion: 1,
      data: {
          status: 'healthy'
      }
  });
};
