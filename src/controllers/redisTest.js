import redis from 'redis';
import config from '../lib/config';

/* eslint no-console: 0 */
let redisClient = null;

function connect(client) {
  redisClient = client;
}
exports.redisConnect = connect;

function testRedis(callback) {
  redisClient = redis.createClient(config.REDIS_PORT, config.REDIS_HOST, { connect_timeout: 10000, max_attempts: 1 });

  redisClient.on('error', (err) => {
    console.log(err);
    callback(err, null);
  });

  redisClient.del('testKey');
  redisClient.set('testKey', 'NotADoctor');

  // Try to retrieve from cache 1
  redisClient.get('testKey', (err, data) => {
      callback(err, data);
  });
}

exports.redisTest = testRedis;

export const redisTestGet = (req, res) => {
  console.log('redisTestGet');
  testRedis((err, data) => {
    if (err) {
      console.log(err);
      res.status(500).end('Redis connection is not working correctly, please check the server logs');
    } else {
      res.status(200).end('Redis is up and reachable!');
    }
  });
};
