import config from '../lib/config';

export const auth = (req, def, scopes, next) => {
   if (!req.headers[def.name]) {
       return next(new Error(`${def.name} header not present`));
   }
   if (req.headers[def.name] === config.NODE_API_KEY) {
       next();
   } else {
       return next(new Error('Not Authorized'));
   }
};
