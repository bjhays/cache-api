#!/bin/sh
rm -rf ./server
npm run "precompile" &
docker stop $(docker ps -a -q) 2> /dev/null &
docker rm $(docker ps -a -q) 2> /dev/null &

wait
docker build --force-rm=true --no-cache=true -t cache-api .
docker run -p 3000:3000 -it cache-api:latest

echo "run 'docker exec -it \$(docker ps | grep \"cache-api:latest\" | awk {'print $1'})  bash'"
