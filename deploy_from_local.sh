eval $(aws ecr get-login --region us-west-2)
# npm install
export $(cat .env | xargs)
env

rm -rf server/*
npm run precompile || exit 1
npm run package
npm run deploy
