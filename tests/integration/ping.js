import chai from 'chai';
import config from '../../server/lib/config';
import swaggerInfo from '../../resources/swagger.json';
import chaiHttp from 'chai-http';
import { server } from '../../server/app';

const expect = chai.expect;
const swaggerPath = swaggerInfo.basePath;

chai.should();
chai.use(chaiHttp);

/* global describe it beforeEach before after */

describe('/ping', () => {
    after(() => {
        server.close();
    });
    it('should respond with 200 to its ping route', (done) => {
        chai.request(server)
            .get(`${swaggerPath}/ping`)
            .set('x-api-key', config.NODE_API_KEY)
            .end((err, res) => {
                expect(res).to.have.status(200);
                done();
            });
    });
});
