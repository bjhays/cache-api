import nock from 'nock';
nock.disableNetConnect();
nock.enableNetConnect(/(127.0.0.1|www.NonExistantServerMock.com*|config.REDIS_HOST|localhost*|nonexistantserver*)/);
