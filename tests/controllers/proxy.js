import config from '../../src/lib/config';
import chai from 'chai';
import chaiHttp from 'chai-http';
import fakeredis from 'fakeredis';
import sinon from 'sinon';
import redis from 'redis';
import nock from 'nock';
import sleep from 'sleep';
import mute from 'mute';

import { redisConnect } from '../../server/controllers/proxy';
import { server } from '../../server/app';


chai.use(chaiHttp);
const expect = chai.expect;
// nock settings in ../a_settings.js
/* global describe it beforeEach before after */

describe('/proxy', () => {
    describe('#when cache is down', () => {
        let redisClient;
        before(() => {
            try {
                sinon.stub(redis, 'createClient', fakeredis.createClient);
            } catch (e) {
                console.log('redis already stubbed');
            }
            redisClient = redis.createClient(config.REDIS_PORT, config.REDIS_HOST, { fast: true });
        });
        beforeEach((done) => {
            nock.cleanAll();
            // console.log('assign redisCLient');
            redisConnect(redisClient);
            redisClient.flushdb(() => {
                done();
            });
        });
        after(() => {
            redis.createClient.restore();
        });

        it('GET: not in whitelist, no redis', (done) => {
            const url = 'www.NonExistantServerMockNoWhite.com';
            const queryParams = `http://${url}`;
            const unmute = mute();
            chai.request(server)
              .get(`/proxy/${queryParams}`)
                .set('Content-Type', 'text/xml; charset=utf-8')
                .end((err, res) => {
                    // if (err) {
                    //     console.log('Test Recieved error');
                    //     console.log(`${err.message}`);
                    // }
                    unmute();
                    expect(res.status).to.equal(500);
                    expect(res.text).to.equal(`${url} not in whitelist`);
                  done();
                });
        });

        it('GET: in whitelist, no redis', (done) => {
            const url = 'www.NonExistantServerB.com';
            const queryParams = `http://${url}`;
            const expected = 'hi!';
            const unmute = mute();

            nock(`http://${url}`)
                .get('/')
                .reply(200, expected);

            chai.request(server)
              .get(`/proxy/${queryParams}`)
              .set('accept-encoding', '*/*')
                .end((err, res) => {
                    unmute();
                    if (err) {
                        console.log('Test Recieved error');
                        console.log(`${err.message}`);
                        console.log(err);
                        expect(false);
                    } else {
                        expect(res.status).to.equal(200);
                        expect(res.text).to.equal(expected);
                        expect(res.headers._proxy_getsource).above(0);
                        expect(res.headers._proxy_proctime).above(0);
                    }
                    done();
                });
        });

        it('GET: in whitelist, no redis, xml', (done) => {
            const url = 'www.NonExistantServerB.com';
            const queryParams = `http://${url}`;
            const expected = '<xml>This is an XML response</xml>';
            const unmute = mute();

            nock(`http://${url}`)
                .get('/')
                .reply(200, expected);

            chai.request(server)
              .get(`/proxy/${queryParams}`)
              .set('accept-encoding', '*/*')
                .end((err, res) => {
                    unmute();
                    if (err) {
                        console.log('Test Recieved error');
                        console.log(`${err.message}`);
                    }
                    expect(res.status).to.equal(200);
                    expect(res.text).to.equal(expected);
                    expect(res.headers._proxy_getsource).above(0);
                    expect(res.headers._proxy_proctime).above(0);
                    done();
                });
        });

        it('POST: not in whitelist, no redis', (done) => {
            const url = 'www.NonExistantServerMockNoWhite.com';
            const queryParams = `http://${url}`;
            const unmute = mute();

            chai.request(server)
              .post(`/proxy/${queryParams}`)
                .set('Content-Type', 'text/xml; charset=utf-8')
                .end((err, res) => {
                    unmute();
                    expect(res.status).to.equal(500);
                    expect(res.text).to.equal(`${url} not in whitelist`);
                  done();
                });
        });

        it('POST: in whitelist, no redis', (done) => {
            const url = 'www.NonExistantServerB.com';
            const queryParams = `http://${url}`;
            const expected = 'hi!';
            const unmute = mute();

            nock(`http://${url}`)
                .post('/')
                .reply(200, expected);

            chai.request(server)
              .post(`/proxy/${queryParams}`)
              .set('accept-encoding', '*/*')
                .end((err, res) => {
                    unmute();
                    if (err) {
                        console.log('Test Recieved error');
                        console.log(`${err.message}`);
                    }
                    expect(res.status).to.equal(200);
                    expect(res.text).to.equal(expected);
                    expect(res.headers._proxy_getsource).above(0);
                    expect(res.headers._proxy_proctime).above(0);
                    done();
                });
        });
    });
    describe('#when cache is up', () => {
        let redisClient;
        before(() => {
            try {
                sinon.stub(redis, 'createClient', fakeredis.createClient);
            } catch (e) {
                console.log('redis already stubbed');
            }
            redisClient = redis.createClient(config.REDIS_PORT, config.REDIS_HOST, { fast: true });
        });
        beforeEach((done) => {
            nock.cleanAll();
            // console.log('assign redisCLient');
            redisConnect(redisClient);
            redisClient.flushdb(() => {
                done();
            });
        });
        after(() => {
            redis.createClient.restore();
        });

        it('GET: not in whitelist, redis', (done) => {
            const url = 'www.NonExistantServerMockNoWhite.com';
            const queryParams = `http://${url}`;
            const unmute = mute();

            chai.request(server)
              .get(`/proxy/${queryParams}`)
                .set('Content-Type', 'text/xml; charset=utf-8')
                .end((err, res) => {
                    unmute();
                    expect(res.status).to.equal(500);
                    expect(res.text).to.equal(`${url} not in whitelist`); // (`{"message":"${url} not in whitelist"`)
                  done();
                });
        });

        it('GET: in whitelist, redis', (done) => {
            const url = 'www.NonExistantServerB.com';
            const queryParams = `http://${url}?cache_duration=10&request_time_out=300`;
            const expected = 'hi!';
            const unmute = mute();

            console.log(`http://${url}`);
            nock(`http://${url}`)
                .get('/')
                .reply(200, expected);

            chai.request(server)
              .get(`/proxy/${queryParams}`)
              .set('accept-encoding', '*/*')
                .end((err, res) => {
                    unmute();
                    expect(res.status).to.equal(200);
                    expect(res.text).to.equal(expected);
                    expect(res.headers._proxy_getsource).above(0);
                    expect(res.headers._proxy_proctime).above(0);
                    chai.request(server)
                      .get(`/proxy/${queryParams}`)
                      .set('accept-encoding', '*/*')
                        .end((err2, res2) => {
                            expect(res2.status).to.equal(200);
                            expect(res2.text).to.equal(expected);
                            expect(res2.headers._proxy_cache).to.equal('L1:hit');
                            expect(res2.headers._proxy_proctime).above(0);
                            done();
                    });
                });
        });

        it('POST: not in whitelist, redis', (done) => {
            const url = 'www.NonExistantServerMockNoWhite.com';
            const queryParams = `http://${url}`;
            const unmute = mute();

            chai.request(server)
              .post(`/proxy/${queryParams}`)
                .set('Content-Type', 'text/xml; charset=utf-8')
                .end((err, res) => {
                    unmute();
                    expect(res.status).to.equal(500);
                    expect(res.text).to.equal(`${url} not in whitelist`);
                  done();
                });
        });

        it('POST: in whitelist, redis', (done) => {
            const url = 'www.NonExistantServerB.com';
            const queryParams = `http://${url}?cache_duration=1&request_time_out=300`;
            const expected = 'hi!';
            const unmute = mute();

            nock(`http://${url}`)
                .post('/')
                .reply(200, expected);

            chai.request(server)
              .post(`/proxy/${queryParams}`)
              .set('accept-encoding', '*/*')
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    expect(res.text).to.equal(expected);
                    expect(res.headers._proxy_getsource).above(0);
                    expect(res.headers._proxy_proctime).above(0);
                    chai.request(server)
                      .post(`/proxy/${queryParams}`)
                      .set('accept-encoding', '*/*')
                        .end((err2, res2) => {
                            unmute();
                            expect(res2.status).to.equal(200);
                            expect(res2.text).to.equal(expected);
                            expect(res2.headers._proxy_cache).to.equal('L1:hit');
                            expect(res2.headers._proxy_proctime).above(0);
                            done();
                    });
                });
        });

        it('GET: in whitelist, redis, xml', (done) => {
            const url = 'www.NonExistantServerB.com';
            const queryParams = `http://${url}`;
            const expected = '<xml>This is an XML response</xml>';
            const unmute = mute();

            nock(`http://${url}`)
                .get('/')
                .reply(200, expected);

            chai.request(server)
              .get(`/proxy/${queryParams}`)
              .set('accept-encoding', '*/*')
                .end((err, res) => {
                    unmute();
                    if (err) {
                        console.log('Test Recieved error');
                        console.log(`${err.message}`);
                    }
                    expect(res.status).to.equal(200);
                    expect(res.text).to.equal(expected);
                    expect(res.headers._proxy_getsource).above(0);
                    expect(res.headers._proxy_proctime).above(0);
                    done();
                });
        });

        it('POST: in whitelist, redis, has queryParams', (done) => {
            const url = 'www.NonExistantServer.com';
            const queryParams = `http://${url}?cache_duration=1&request_time_out=300&a=1&b=2`;
            const expected = 'hi!';
            const unmute = mute();

            nock(`http://${url}`)
                .post('/')
                .reply(200, expected);

            chai.request(server)
              .post(`/proxy/${queryParams}`)
              .set('accept-encoding', '*/*')
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    expect(res.text).to.equal(expected);
                    expect(res.headers._proxy_getsource).above(0);
                    expect(res.headers._proxy_proctime).above(0);
                    chai.request(server)
                      .post(`/proxy/${queryParams}`)
                      .set('accept-encoding', '*/*')
                        .end((err2, res2) => {
                            unmute();
                            if (err2) {
                                console.log(err2);
                            }
                            // console.log(res2);
                            expect(res2.status).to.equal(200);
                            expect(res2.text).to.equal(expected);
                            expect(res2.headers._proxy_cache).to.equal('L1:hit');
                            expect(res2.headers._proxy_proctime).above(0);
                            done();
                    });
                });
        });

        it('POST: in whitelist, redis, has queryParams, L2 only', (done) => {
            const url = 'www.NonExistantServer.com';
            const queryParams = `http://${url}?cache_duration=1&request_time_out=300&a=1&b=2`;
            const expected = 'hi!';
            const unmute = mute();

            nock(`http://${url}`)
                .post('/')
                .reply(200, expected);

            chai.request(server)
              .post(`/proxy/${queryParams}`)
              .set('accept-encoding', '*/*')
                .end((err, res) => {
                    try {
                        expect(res.status).to.equal(200);
                        expect(res.text).to.equal(expected);
                        // expect(res.headers._proxy_getsource).above(0);
                        expect(res.headers._proxy_proctime).above(0);
                    } catch (e) {
                        console.log(res.status);
                        console.log(res.text);
                        console.log(res.headers);
                        throw e;
                    }
                    // allow the cache to expire
                    console.log('turn off network, sleep');
                    // nock.disableNetConnect();
                    sleep.sleep(2);
                    chai.request(server)
                      .post(`/proxy/${queryParams}`)
                      .set('accept-encoding', '*/*')
                        .end((err2, res2) => {
                            // console.log(res2);
                            unmute();
                            try {
                                expect(res2.status).to.equal(200);
                                expect(res2.text).to.equal(expected);
                                expect(res2.headers._proxy_cache).to.equal('L2:hit');
                                expect(res2.headers._proxy_proctime).above(0);
                            } catch (e) {
                                console.log('debugging:');
                                console.log(res2.status);
                                console.log(res2.text);
                                console.log(res2.headers);
                                throw e;
                            }
                            // nock.enableNetConnect();
                            done();
                    });
                });
        });
    });
});
